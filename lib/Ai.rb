require "rubygems"
require "ai4r"

class Ai
	attr_reader :network

	def initialize(input_size, neurons)
		@network = Ai4r::NeuralNetwork::Backpropagation.new([input_size, neurons, 1])
	end

	def train(inputs, results, training_times=500)
		raise "Input size must be the same as results size" unless inputs.size == results.size

		training_times.times do |i|
			inputs.size.times do |j|
				@network.train(inputs[j], results[j])
			end
		end
	end

	def evaluate(input)
		@network.eval(input)
	end

end