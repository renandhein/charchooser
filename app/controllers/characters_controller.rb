class CharactersController < ApplicationController

  def index
    if params["mix"].present?
      @display_characters = Character.new.characters.shuffle[0..2]
    else
      @counter = params["counter"].present? ? params["counter"] : 0
      @display_characters = Character.new.characters[@counter.to_i..(@counter.to_i + 2)]
    end
  end

  def evaluate
    # Creates the AI that contains the network inside the session
    session[:ai] ||= Ai.new(15, 15)
    # Find the characters present in the screen
    characters = Character.new.characters.select{|e| params["current_chars"].include?(e[:id].to_s) }
    # Gathers the attributes of the characters and prepares it for the network
    inputs = characters.map{|e| e[:attributes]}
    if params["character"].present?
      # Adjusts the results based on the chosen character between the present ones
      results = characters.map{|e| [(e[:id].to_s == params["character"]) ? 1 : 0]}
      # Train the network with the inputs and results
      session[:ai].train(inputs, results, 500)
    end

    redirect_to(action: :index, params: {counter: (params["counter"].to_i + 3)})
  end

  def reset
    # Erases the AI stored in the session
    session[:ai] = nil

    redirect_to(action: :index, params: {counter: 0})
  end

  def mix
    redirect_to(action: :index, params: {counter: 0, mix: true})
  end

  def choose
    # Find the characters present in the screen
    characters = Character.new.characters.select{|e| params["current_chars"].include?(e[:id].to_s) }
    # Let the network evaluate each one of the character's attributes
    results = characters.map{|e| session[:ai].evaluate(e[:attributes]) }.flatten

    render :json => {chosen: characters[results.index(results.max)][:id].to_s}
  end

end
