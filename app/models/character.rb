class Character
	attr_accessor :characters

	def initialize
		@characters = []
		@characters << {
			:id => 1, 
			:image => "char1.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.75, # 1 (stylish-looking) / 0 (not stylish)
				0.10, # 1 (unusual weapon)  / 0 (normal weapon)
				0.5,  # 1 (big weapon)      / 0 (no weapon)
				1,    # 1 (good looking)    / 0 (ugly as hell)
				0.95, # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.7,  # 1 (agile)           / 0 (slow)
				0.8,  # 1 (good)            / 0 (evil)
				0.3,  # 1 (unusual)         / 0 (common)
				0.6,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 2, 
			:image => "char2.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.85, # 1 (stylish-looking) / 0 (not stylish)
				0.7,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.9,  # 1 (big weapon)      / 0 (no weapon)
				0.6,  # 1 (good looking)    / 0 (ugly as hell)
				0.8,  # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0,    # 1 (agile)           / 0 (slow)
				0.5,  # 1 (good)            / 0 (evil)
				0.6,  # 1 (unusual)         / 0 (common)
				1,    # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				0.9   # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 3, 
			:image => "char3.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.1,  # 1 (stylish-looking) / 0 (not stylish)
				0,    # 1 (unusual weapon)  / 0 (normal weapon)
				0.3,  # 1 (big weapon)      / 0 (no weapon)
				0.2,  # 1 (good looking)    / 0 (ugly as hell)
				0,    # 1 (warrior-like)    / 0 (not warrior-like)
				1,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.6,  # 1 (agile)           / 0 (slow)
				0.2,  # 1 (good)            / 0 (evil)
				0.8,  # 1 (unusual)         / 0 (common)
				0.5,  # 1 (strong looking)  / 0 (weak looking)
				0.6,  # 1 (black skin)      / 0 (white skin)
				0.6   # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 4, 
			:image => "char4.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.2,  # 1 (stylish-looking) / 0 (not stylish)
				0,    # 1 (unusual weapon)  / 0 (normal weapon)
				0.1,  # 1 (big weapon)      / 0 (no weapon)
				0.4,  # 1 (good looking)    / 0 (ugly as hell)
				0.8,  # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.7,  # 1 (agile)           / 0 (slow)
				0.2,  # 1 (good)            / 0 (evil)
				0.8,  # 1 (unusual)         / 0 (common)
				0.6,  # 1 (strong looking)  / 0 (weak looking)
				0.6,  # 1 (black skin)      / 0 (white skin)
				0.6   # 1 (young)           / 0 (old)
			]
		}			
		@characters << {
			:id => 5, 
			:image => "char5.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				0.9,  # 1 (stylish-looking) / 0 (not stylish)
				0.3,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.6,  # 1 (big weapon)      / 0 (no weapon)
				1,    # 1 (good looking)    / 0 (ugly as hell)
				0,    # 1 (warrior-like)    / 0 (not warrior-like)
				1,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.7,  # 1 (agile)           / 0 (slow)
				1,    # 1 (good)            / 0 (evil)
				0.4,  # 1 (unusual)         / 0 (common)
				0.6,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 6, 
			:image => "char6.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.5,  # 1 (stylish-looking) / 0 (not stylish)
				0.7,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.7,  # 1 (big weapon)      / 0 (no weapon)
				0.4,  # 1 (good looking)    / 0 (ugly as hell)
				0,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				1,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.6,  # 1 (agile)           / 0 (slow)
				0.4,  # 1 (good)            / 0 (evil)
				0.6,  # 1 (unusual)         / 0 (common)
				0.7,  # 1 (strong looking)  / 0 (weak looking)
				0.8,  # 1 (black skin)      / 0 (white skin)
				0     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 7, 
			:image => "char7.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				0.5,  # 1 (stylish-looking) / 0 (not stylish)
				0.4,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.5,  # 1 (big weapon)      / 0 (no weapon)
				0.4,  # 1 (good looking)    / 0 (ugly as hell)
				0.5,  # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0.5,  # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.6,  # 1 (agile)           / 0 (slow)
				0.4,  # 1 (good)            / 0 (evil)
				0.6,  # 1 (unusual)         / 0 (common)
				0.5,  # 1 (strong looking)  / 0 (weak looking)
				0.8,  # 1 (black skin)      / 0 (white skin)
				0.2   # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 8, 
			:image => "char8.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				1,    # 1 (stylish-looking) / 0 (not stylish)
				0.7,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.5,  # 1 (big weapon)      / 0 (no weapon)
				1,    # 1 (good looking)    / 0 (ugly as hell)
				0,    # 1 (warrior-like)    / 0 (not warrior-like)
				1,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.8,  # 1 (agile)           / 0 (slow)
				0.8,  # 1 (good)            / 0 (evil)
				0.5,  # 1 (unusual)         / 0 (common)
				0.5,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 9, 
			:image => "char9.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				0.7,  # 1 (stylish-looking) / 0 (not stylish)
				1,    # 1 (unusual weapon)  / 0 (normal weapon)
				0.8,  # 1 (big weapon)      / 0 (no weapon)
				0.8,  # 1 (good looking)    / 0 (ugly as hell)
				1,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.3,  # 1 (agile)           / 0 (slow)
				0.8,  # 1 (good)            / 0 (evil)
				0.8,  # 1 (unusual)         / 0 (common)
				0.6,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 10, 
			:image => "char10.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				1,    # 1 (stylish-looking) / 0 (not stylish)
				0.6,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.2,  # 1 (big weapon)      / 0 (no weapon)
				1,    # 1 (good looking)    / 0 (ugly as hell)
				1,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.7,  # 1 (agile)           / 0 (slow)
				0.1,  # 1 (good)            / 0 (evil)
				0.7,  # 1 (unusual)         / 0 (common)
				1,    # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 11, 
			:image => "char11.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.8,  # 1 (stylish-looking) / 0 (not stylish)
				0.2,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.3,  # 1 (big weapon)      / 0 (no weapon)
				1,    # 1 (good looking)    / 0 (ugly as hell)
				0,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				1,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.8,  # 1 (agile)           / 0 (slow)
				0.9,  # 1 (good)            / 0 (evil)
				0.2,  # 1 (unusual)         / 0 (common)
				0.8,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				0.7   # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 12, 
			:image => "char12.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0,    # 1 (stylish-looking) / 0 (not stylish)
				0.7,  # 1 (unusual weapon)  / 0 (normal weapon)
				0,    # 1 (big weapon)      / 0 (no weapon)
				0,    # 1 (good looking)    / 0 (ugly as hell)
				0.5,  # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0,    # 1 (agile)           / 0 (slow)
				0,    # 1 (good)            / 0 (evil)
				0.7,  # 1 (unusual)         / 0 (common)
				0,    # 1 (strong looking)  / 0 (weak looking)
				1,    # 1 (black skin)      / 0 (white skin)
				0     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 13, 
			:image => "char13.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.7,  # 1 (stylish-looking) / 0 (not stylish)
				0.8,  # 1 (unusual weapon)  / 0 (normal weapon)
				0,    # 1 (big weapon)      / 0 (no weapon)
				0.5,  # 1 (good looking)    / 0 (ugly as hell)
				0,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				1,    # 1 (ranger-like)     / 0 (not ranger-like)
				0,    # 1 (agile)           / 0 (slow)
				0,    # 1 (good)            / 0 (evil)
				0.7,  # 1 (unusual)         / 0 (common)
				0.5,  # 1 (strong looking)  / 0 (weak looking)
				0.5,  # 1 (black skin)      / 0 (white skin)
				0.6   # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 14, 
			:image => "char14.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				1,    # 1 (stylish-looking) / 0 (not stylish)
				0.9,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.8,  # 1 (big weapon)      / 0 (no weapon)
				0.5,  # 1 (good looking)    / 0 (ugly as hell)
				1,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.5,  # 1 (agile)           / 0 (slow)
				0,    # 1 (good)            / 0 (evil)
				0.8,  # 1 (unusual)         / 0 (common)
				1,    # 1 (strong looking)  / 0 (weak looking)
				0.7,  # 1 (black skin)      / 0 (white skin)
				0.4   # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 15, 
			:image => "char15.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				0.8,  # 1 (stylish-looking) / 0 (not stylish)
				0.9,  # 1 (unusual weapon)  / 0 (normal weapon)
				0,    # 1 (big weapon)      / 0 (no weapon)
				1,    # 1 (good looking)    / 0 (ugly as hell)
				0,    # 1 (warrior-like)    / 0 (not warrior-like)
				0.6,  # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.9,  # 1 (agile)           / 0 (slow)
				0.7,  # 1 (good)            / 0 (evil)
				1,    # 1 (unusual)         / 0 (common)
				0.3,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 16, 
			:image => "char16.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.4,  # 1 (stylish-looking) / 0 (not stylish)
				0.5,  # 1 (unusual weapon)  / 0 (normal weapon)
				0,    # 1 (big weapon)      / 0 (no weapon)
				0.5,  # 1 (good looking)    / 0 (ugly as hell)
				0,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				1,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.9,  # 1 (agile)           / 0 (slow)
				0.3,  # 1 (good)            / 0 (evil)
				0.4,  # 1 (unusual)         / 0 (common)
				0.8,  # 1 (strong looking)  / 0 (weak looking)
				0.7,  # 1 (black skin)      / 0 (white skin)
				0.4   # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 17, 
			:image => "char17.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				1,    # 1 (stylish-looking) / 0 (not stylish)
				0.7,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.7,  # 1 (big weapon)      / 0 (no weapon)
				1,    # 1 (good looking)    / 0 (ugly as hell)
				1,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.4,  # 1 (agile)           / 0 (slow)
				1,    # 1 (good)            / 0 (evil)
				0.3,  # 1 (unusual)         / 0 (common)
				0.8,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 18, 
			:image => "char18.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				1,    # 1 (stylish-looking) / 0 (not stylish)
				0.3,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.2,  # 1 (big weapon)      / 0 (no weapon)
				1,    # 1 (good looking)    / 0 (ugly as hell)
				0.5,  # 1 (warrior-like)    / 0 (not warrior-like)
				0.5,  # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.6,  # 1 (agile)           / 0 (slow)
				0,    # 1 (good)            / 0 (evil)
				0.8,  # 1 (unusual)         / 0 (common)
				1,    # 1 (strong looking)  / 0 (weak looking)
				0.8,  # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 19, 
			:image => "char19.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				1,    # 1 (stylish-looking) / 0 (not stylish)
				0.7,  # 1 (unusual weapon)  / 0 (normal weapon)
				0,    # 1 (big weapon)      / 0 (no weapon)
				1,    # 1 (good looking)    / 0 (ugly as hell)
				0.5,  # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				1,    # 1 (agile)           / 0 (slow)
				0.3,  # 1 (good)            / 0 (evil)
				0.8,  # 1 (unusual)         / 0 (common)
				0.7,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 20, 
			:image => "char20.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.7,  # 1 (stylish-looking) / 0 (not stylish)
				1,    # 1 (unusual weapon)  / 0 (normal weapon)
				0.5,  # 1 (big weapon)      / 0 (no weapon)
				0.8,  # 1 (good looking)    / 0 (ugly as hell)
				0.8,  # 1 (warrior-like)    / 0 (not warrior-like)
				0.5,  # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.2,  # 1 (agile)           / 0 (slow)
				0.5,  # 1 (good)            / 0 (evil)
				1,    # 1 (unusual)         / 0 (common)
				0.5,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 21, 
			:image => "char21.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				0.6,  # 1 (stylish-looking) / 0 (not stylish)
				0,    # 1 (unusual weapon)  / 0 (normal weapon)
				0.2,  # 1 (big weapon)      / 0 (no weapon)
				0.8,  # 1 (good looking)    / 0 (ugly as hell)
				1,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.8,  # 1 (agile)           / 0 (slow)
				1,    # 1 (good)            / 0 (evil)
				0.2,  # 1 (unusual)         / 0 (common)
				0.4,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 22, 
			:image => "char22.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				1,    # 1 (stylish-looking) / 0 (not stylish)
				1,    # 1 (unusual weapon)  / 0 (normal weapon)
				0,    # 1 (big weapon)      / 0 (no weapon)
				0.9,  # 1 (good looking)    / 0 (ugly as hell)
				0.5,  # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				1,    # 1 (agile)           / 0 (slow)
				0.2,  # 1 (good)            / 0 (evil)
				1,    # 1 (unusual)         / 0 (common)
				0.8,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 23, 
			:image => "char23.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				1,    # 1 (stylish-looking) / 0 (not stylish)
				1,    # 1 (unusual weapon)  / 0 (normal weapon)
				0,    # 1 (big weapon)      / 0 (no weapon)
				1,    # 1 (good looking)    / 0 (ugly as hell)
				0,    # 1 (warrior-like)    / 0 (not warrior-like)
				1,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.6,  # 1 (agile)           / 0 (slow)
				0,    # 1 (good)            / 0 (evil)
				1,    # 1 (unusual)         / 0 (common)
				1,    # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 24, 
			:image => "char24.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.3,  # 1 (stylish-looking) / 0 (not stylish)
				0,    # 1 (unusual weapon)  / 0 (normal weapon)
				0.3,  # 1 (big weapon)      / 0 (no weapon)
				0.8,  # 1 (good looking)    / 0 (ugly as hell)
				1,    # 1 (warrior-like)    / 0 (not warrior-like)
				0.2,  # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.4,  # 1 (agile)           / 0 (slow)
				1,    # 1 (good)            / 0 (evil)
				0,    # 1 (unusual)         / 0 (common)
				0.5,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				0.5   # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 25, 
			:image => "char25.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.1,  # 1 (stylish-looking) / 0 (not stylish)
				0.3,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.5,  # 1 (big weapon)      / 0 (no weapon)
				0,    # 1 (good looking)    / 0 (ugly as hell)
				1,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.1,  # 1 (agile)           / 0 (slow)
				0,    # 1 (good)            / 0 (evil)
				0.3,  # 1 (unusual)         / 0 (common)
				0.6,  # 1 (strong looking)  / 0 (weak looking)
				0.7,  # 1 (black skin)      / 0 (white skin)
				0.2   # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 26, 
			:image => "char26.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.4,  # 1 (stylish-looking) / 0 (not stylish)
				0,    # 1 (unusual weapon)  / 0 (normal weapon)
				0.5,  # 1 (big weapon)      / 0 (no weapon)
				0.7,  # 1 (good looking)    / 0 (ugly as hell)
				1,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0.3,  # 1 (ranger-like)     / 0 (not ranger-like)
				0.5,  # 1 (agile)           / 0 (slow)
				0.8,  # 1 (good)            / 0 (evil)
				0.1,  # 1 (unusual)         / 0 (common)
				0.6,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				0.8   # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 27, 
			:image => "char27.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				0.8,  # 1 (stylish-looking) / 0 (not stylish)
				0.2,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.3,  # 1 (big weapon)      / 0 (no weapon)
				0.9,  # 1 (good looking)    / 0 (ugly as hell)
				0,    # 1 (warrior-like)    / 0 (not warrior-like)
				1,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.8,  # 1 (agile)           / 0 (slow)
				0.9,  # 1 (good)            / 0 (evil)
				0.1,  # 1 (unusual)         / 0 (common)
				0.4,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 28, 
			:image => "char28.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.2,  # 1 (stylish-looking) / 0 (not stylish)
				0.2,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.4,  # 1 (big weapon)      / 0 (no weapon)
				0.3,  # 1 (good looking)    / 0 (ugly as hell)
				1,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.2,  # 1 (agile)           / 0 (slow)
				0.9,  # 1 (good)            / 0 (evil)
				0.6,  # 1 (unusual)         / 0 (common)
				0.4,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				0.2   # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 29, 
			:image => "char29.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				0.7,  # 1 (stylish-looking) / 0 (not stylish)
				0.8,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.2,  # 1 (big weapon)      / 0 (no weapon)
				0.9,  # 1 (good looking)    / 0 (ugly as hell)
				1,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.9,  # 1 (agile)           / 0 (slow)
				1,    # 1 (good)            / 0 (evil)
				0.8,  # 1 (unusual)         / 0 (common)
				0.5,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 30, 
			:image => "char30.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0.5,  # 1 (stylish-looking) / 0 (not stylish)
				0.2,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.4,  # 1 (big weapon)      / 0 (no weapon)
				0.8,  # 1 (good looking)    / 0 (ugly as hell)
				1,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.7,  # 1 (agile)           / 0 (slow)
				0.2,  # 1 (good)            / 0 (evil)
				0.3,  # 1 (unusual)         / 0 (common)
				0.8,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				0.7   # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 31, 
			:image => "char31.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				0.7,  # 1 (stylish-looking) / 0 (not stylish)
				0.8,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.7,  # 1 (big weapon)      / 0 (no weapon)
				0.5,  # 1 (good looking)    / 0 (ugly as hell)
				0.8,  # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.4,  # 1 (agile)           / 0 (slow)
				0.7,  # 1 (good)            / 0 (evil)
				0.6,  # 1 (unusual)         / 0 (common)
				0.5,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				0.9   # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 32, 
			:image => "char32.jpg",
			:attributes => [
				0,    # 1 (male)            / 0 (female)
				0.9,  # 1 (stylish-looking) / 0 (not stylish)
				0.3,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.3,  # 1 (big weapon)      / 0 (no weapon)
				1,   # 1 (good looking)    / 0 (ugly as hell)
				0.9,  # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0.9,  # 1 (agile)           / 0 (slow)
				0.5,  # 1 (good)            / 0 (evil)
				0.8,  # 1 (unusual)         / 0 (common)
				0.9,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				1     # 1 (young)           / 0 (old)
			]
		}
		@characters << {
			:id => 33, 
			:image => "char33.jpg",
			:attributes => [
				1,    # 1 (male)            / 0 (female)
				0,    # 1 (stylish-looking) / 0 (not stylish)
				0.2,  # 1 (unusual weapon)  / 0 (normal weapon)
				0.2,  # 1 (big weapon)      / 0 (no weapon)
				0,    # 1 (good looking)    / 0 (ugly as hell)
				1,    # 1 (warrior-like)    / 0 (not warrior-like)
				0,    # 1 (mage-like)       / 0 (not mage-like)
				0,    # 1 (monk-like)       / 0 (not monk-like)
				0,    # 1 (ranger-like)     / 0 (not ranger-like)
				0,    # 1 (agile)           / 0 (slow)
				0,    # 1 (good)            / 0 (evil)
				0.6,  # 1 (unusual)         / 0 (common)
				0.1,  # 1 (strong looking)  / 0 (weak looking)
				0,    # 1 (black skin)      / 0 (white skin)
				0.2   # 1 (young)           / 0 (old)
			]
		}
	end

end
